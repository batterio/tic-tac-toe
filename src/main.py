# Import modules
import time
from sense_hat import SenseHat

sense = SenseHat()
sense.show_message("NALA")
time.sleep(1)

# Global Variables
pointer_color = (255, 255, 0)  # Yellow
player_0_color = (0, 255, 0)  # Green
player_1_color = (0, 255, 255)  # Cyan
line_colors = (255, 0, 0)  # Red
empty_color = (0, 0, 0)  # Black
player = 0
pressed = False


def init():
    grid = [
        0, 0, -1, 0, 0, -1, 0, 0,
        0, 0, -1, 0, 0, -1, 0, 0,
        -1, -1, -1, -1, -1, -1, -1, -1,
        0, 0, -1, 0, 0, -1, 0, 0,
        0, 0, -1, 0, 0, -1, 0, 0,
        -1, -1, -1, -1, -1, -1, -1, -1,
        0, 0, -1, 0, 0, -1, 0, 0,
        0, 0, -1, 0, 0, -1, 0, 0,
    ]

    background = [
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    ]
    pointer = {'x': 0, 'y': 0}
    return grid, background, pointer


def grid_rows():
    return [[0, 3, 6], [24, 27, 30], [48, 51, 54]]


def grid_cols():
    return [[0, 24, 48], [3, 27, 51], [6, 30, 5]]


def grid_diag():
    return [[0, 27, 54], [6, 27, 48]]


def check_win():
    for axes in (grid_rows(), grid_cols(), grid_diag()):
        for r in axes:
            for p in (1, 2):
                if all([grid[i] == p for i in r]):
                    game_over(player=p, coords=r)
                    return True
    return False


def check_tied():
    if grid.count(0) == 0:
        return True
    return False


def check_shake():
    acceleration = sense.get_accelerometer_raw()
    x = abs(acceleration['x'])
    y = abs(acceleration['y'])
    z = abs(acceleration['z'])

    if x > 2 or y > 2 or z > 2:
        sense.clear()
        sense.show_message("NALA")
        return True
    return False


def game_over(player, coords):
    player_color = player_0_color if player == 1 else player_1_color
    for i in range(3):
        for color in (empty_color, player_color):
            for r in coords:
                for n in (0, 1, 8, 9):
                    background[r + n] = color
            sense.low_light = True  # Optional
            sense.set_pixels(background)
            time.sleep(0.3)


grid, background, pointer = init()

while True:

    for event in sense.stick.get_events():
        if event.action == 'pressed':
            if event.direction == 'down':
                pointer['y'] += 3 if pointer['y'] < 6 else 0
            elif event.direction == 'up':
                pointer['y'] -= 3 if pointer['y'] > 2 else 0
            elif event.direction == 'right':
                pointer['x'] += 3 if pointer['x'] < 6 else 0
            elif event.direction == 'left':
                pointer['x'] -= 3 if pointer['x'] > 2 else 0
            elif event.direction == 'middle':
                pressed = True

    if pressed:
        pressed = False
        if grid[pointer['y'] * 8 + pointer['x']] == 0:
            player = abs(player - 1)
            for n in (0, 1, 8, 9):
                grid[pointer['y'] * 8 + pointer['x'] + n] = 1 if player == 0 else 2

    for index in range(0, 64):
        if grid[index] == -1:
            background[index] = line_colors
        elif grid[index] == 0:
            background[index] = empty_color
        elif grid[index] == 1:
            background[index] = player_0_color
        else:
            background[index] = player_1_color

    if grid[pointer['y'] * 8 + pointer['x']] == 0:
        for n in (0, 1, 8, 9):
            background[pointer['y'] * 8 + pointer['x'] + n] = pointer_color
    else:
        background[pointer['y'] * 8 + pointer['x']] = pointer_color

    # Display the time
    sense.low_light = True  # Optional
    sense.set_pixels(background)
    time.sleep(0.1)

    if check_win() or check_tied() or check_shake():
        grid, background, pointer = init()
